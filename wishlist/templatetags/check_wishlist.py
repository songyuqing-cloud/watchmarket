from django import template

from wishlist.models import WishListItemModel

register = template.Library()


# filtro utilizzato per decidere se far vedere o meno il bottone di aggiunta alla wishlist
@register.filter
def is_in_wishlist(user, obj):
    # se esiste l'oggetto ritorno true, altrimenti false
    if len(WishListItemModel.objects.filter(wishlist_owner=user, wishlist_watch=obj)):
        return True
    return False
