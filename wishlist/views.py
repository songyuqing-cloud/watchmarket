from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView

from watches.models import WatchSaleAnnouncementModel
from wishlist.models import WishListItemModel


class WishListView(LoginRequiredMixin, ListView):
    model = WishListItemModel
    template_name = 'wishlist/wishlist_list.html'
    paginate_by = 10

    def get_queryset(self):
        # così ottengo soltanto quelli dell'utente corrente
        return WishListItemModel.objects.filter(
            wishlist_owner=self.request.user
        ).order_by('create_date')


@login_required
def add_to_wishlist(request, **kwargs):
    # ottengo l'utente loggato, l'orologio su cui ha premuto e infine lo aggiungo nel db
    watch = WatchSaleAnnouncementModel.objects.get(pk=kwargs['pk'])
    user = request.user

    wish = WishListItemModel(wishlist_watch=watch, wishlist_owner=user)
    wish.save()

    messages.success(request, 'Item added to wishlist!')

    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def remove_from_wishlist(request, **kwargs):
    watch = WatchSaleAnnouncementModel.objects.get(pk=kwargs['pk'])
    user = request.user

    # in questo caso dovrebbe essere unico, in quanto non ci sono username uguali
    wish = WishListItemModel.objects.get(wishlist_watch=watch, wishlist_owner=user)
    wish.delete()

    messages.success(request, 'Item removed from wishlist!')

    return redirect(request.META.get('HTTP_REFERER'))
