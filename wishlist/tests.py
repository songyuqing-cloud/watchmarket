from django.test import Client, TestCase

# Create your tests here.
from django.urls import reverse_lazy

from profiles.models import UserProfileModel
from watches.models import WatchSaleAnnouncementModel
from wishlist.models import WishListItemModel


class WishListTest(TestCase):
    def setUp(self):
        self.image_path = 'watches/photos/rolex.jpg'
        self.created_seller = UserProfileModel.objects.create_user('provaseller',
                                                                   email='prova@prova.it',
                                                                   password='tentativo',
                                                                   is_seller=True)
        self.created_seller.save()

        self.created_seller2 = UserProfileModel.objects.create_user('provaseller2',
                                                                    email='prova@prova.it',
                                                                    password='tentativo',
                                                                    is_seller=True)
        self.created_seller2.save()

        self.created_user = UserProfileModel.objects.create_user('provauser',
                                                                 email='prova@prova.it',
                                                                 password='tentativo')
        self.created_user.save()

        self.watch = WatchSaleAnnouncementModel.objects.create(watch_brand='Rolex',
                                                               watch_model='Datejust',
                                                               watch_price=5000.0,
                                                               watch_photos=self.image_path,
                                                               watch_year=2018,
                                                               watch_condition='VERY_GOOD',
                                                               watch_size=39,
                                                               watch_case_material='STAINLESS_STEEL',
                                                               watch_bracelet_material='STAINLESS_STEEL',
                                                               watch_seller=self.created_seller)
        self.watch.save()

        self.watch2 = WatchSaleAnnouncementModel.objects.create(watch_brand='Rolex',
                                                                watch_model='DayDate',
                                                                watch_price=25000.0,
                                                                watch_photos=self.image_path,
                                                                watch_year=2015,
                                                                watch_condition='VERY_GOOD',
                                                                watch_size=36,
                                                                watch_case_material='STAINLESS_STEEL',
                                                                watch_bracelet_material='STAINLESS_STEEL',
                                                                watch_seller=self.created_seller2)

        self.wishlist_item = WishListItemModel.objects.create(wishlist_watch=self.watch2,
                                                              wishlist_owner=self.created_user)

    def test_wishlist_not_empty(self):
        client = Client()
        response = client.login(username='provauser', password='tentativo')
        self.assertTrue(response)

        response = client.get(reverse_lazy('wishlist:wishlist-all'))

        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(len(response.context['object_list']), 0)

    def test_wishlist_empty(self):
        client = Client()
        response = client.login(username='provaseller', password='tentativo')
        self.assertTrue(response)

        response = client.get(reverse_lazy('wishlist:wishlist-all'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 0)

    def test_wishlist_add(self):
        client = Client()

        response = client.login(username='provauser', password='tentativo')

        self.assertTrue(response)

        response = client.post(reverse_lazy('wishlist:wishlist-add', kwargs={'pk': self.watch.pk}),
                               follow=True,
                               HTTP_REFERER=reverse_lazy('home'))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Item added to wishlist!')

        response = client.get(reverse_lazy('wishlist:wishlist-all'))

        self.assertContains(response, self.watch.watch_brand + ' ' + self.watch.watch_model)

    def test_wishlist_add_not_logged(self):
        client = Client()

        response = client.post(reverse_lazy('wishlist:wishlist-add', kwargs={'pk': self.watch.pk}),
                               follow=True,
                               HTTP_REFERER=reverse_lazy('home'))

        self.assertContains(response, 'login')

    def test_wishlist_remove(self):
        client = Client()

        response = client.login(username='provauser', password='tentativo')

        self.assertTrue(response)

        response = client.post(reverse_lazy('wishlist:wishlist-remove', kwargs={'pk': self.watch2.pk}),
                               follow=True,
                               HTTP_REFERER=reverse_lazy('home'))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Item removed from wishlist!')

    def test_watch_does_not_exists(self):
        client = Client()

        response = client.login(username='provauser', password='tentativo')

        self.assertTrue(response)

        try:
            response = client.post(reverse_lazy('wishlist:wishlist-add', kwargs={'pk': 50}),
                                   follow=True,
                                   HTTP_REFERER=reverse_lazy('home'))
            self.fail('No exception has been raised')
        except WatchSaleAnnouncementModel.DoesNotExist:
            pass
