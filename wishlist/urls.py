from django.urls import path

from wishlist.views import WishListView, add_to_wishlist, remove_from_wishlist

app_name = 'wishlist'


urlpatterns = [
    path('list/', WishListView.as_view(), name='wishlist-all'),
    path('<int:pk>/add/', add_to_wishlist, name='wishlist-add'),
    path('<int:pk>/remove/', remove_from_wishlist, name='wishlist-remove')
]