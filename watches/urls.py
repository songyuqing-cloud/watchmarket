from django.urls import path# nel contesto inserisco tutta la ricerca per poter ripopolare i campi e far andare la pagination

from watches.views import WatchSaleAnnouncementView, WatchSalePostView, SaleAnnouncementList, PersonalSaleAnnouncement, \
    WatchSaleAnnouncementUpdate, WatchSaleAnnouncementDelete, WatchSearchView, ProfileShopView, ConfirmBuyView, \
    BoughtView

app_name = 'watches'

urlpatterns = [
    path('sell/', WatchSaleAnnouncementView.as_view(), name='insert_sale_announcement'),
    path('announcement/<int:pk>/detail/', WatchSalePostView.as_view(), name='sale_announcement_detail'),
    path('announcement/<int:pk>/update/', WatchSaleAnnouncementUpdate.as_view(), name='sale_announcement_update'),
    path('announcement/<int:pk>/delete/', WatchSaleAnnouncementDelete.as_view(), name='sale_announcement_delete'),
    path('list/', SaleAnnouncementList.as_view(), name='sale_announcement_list'),
    path('list/search/', WatchSearchView.as_view(), name='sale_announcement_search_list'),
    path('mylist/', PersonalSaleAnnouncement.as_view(), name='my_sale_announcement_list'),
    path('<int:pk>/shop/', ProfileShopView.as_view(), name='sale_shop_list'),
    path('<int:pk>/confirmation/', ConfirmBuyView.as_view(), name='confirm_purchase'),
    path('<int:pk>/bought/', BoughtView.as_view(), name='watch_bought'),
]
