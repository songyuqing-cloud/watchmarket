from django import template

register = template.Library()


@register.filter
def is_owner(user, obj):
    if user.is_authenticated and obj.watch_seller == user:
        return True
    return False
