from watches.models import WatchSaleAnnouncementModel


# mixin per controllare che l'utente che fa la richiesta sia un venditore
class IsSellerMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_seller:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


# mixin per controllare che l'utente che fa la richiesta sia il proprietario dell'orologio
class IsOwnerMixin:
    def dispatch(self, request, *args, **kwargs):
        watch = WatchSaleAnnouncementModel.objects.get(pk=kwargs['pk'])
        if not request.user == watch.watch_seller:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


# mixin per controllare che al momento di un acquisto l'utente che fa la richiesta non sia il proprietario dell'oggetto
# (ad esempio quando c'è un acquisto)
class IsNotOwnerMixin:
    def dispatch(self, request, *args, **kwargs):
        watch = WatchSaleAnnouncementModel.objects.get(pk=kwargs['pk'])
        if not request.user != watch.watch_seller:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
