from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from reviews.models import ReviewModel


class LeaveReviewForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_id = 'leave_review_form'
    helper.form_method = 'POST'
    helper.add_input(Submit('save', 'Save'))
    helper.inputs[0].field_classes = 'btn btn-success'

    class Meta:
        model = ReviewModel
        fields = [
            'review_seller_rating',
            'review_title',
            'review_content',
        ]
