from django.test import Client, TestCase


# Create your tests here.
from django.urls import reverse_lazy

from profiles.models import UserProfileModel
from reviews.models import ReviewModel
from watches.models import WatchSaleAnnouncementModel


class ReviewsTest(TestCase):
    def setUp(self):
        self.image_path = 'watches/photos/rolex.jpg'
        self.created_seller = UserProfileModel.objects.create_user('provaseller',
                                                                   email='prova@prova.it',
                                                                   password='tentativo',
                                                                   is_seller=True)
        self.created_seller.save()

        self.created_seller2 = UserProfileModel.objects.create_user('provaseller2',
                                                                    email='prova@prova.it',
                                                                    password='tentativo',
                                                                    is_seller=True)
        self.created_seller2.save()

        self.created_user = UserProfileModel.objects.create_user('provauser',
                                                                 email='prova@prova.it',
                                                                 password='tentativo')
        self.created_user.save()

        self.watch = WatchSaleAnnouncementModel.objects.create(watch_brand='Rolex',
                                                               watch_model='Datejust',
                                                               watch_price=5000.0,
                                                               watch_photos=self.image_path,
                                                               watch_year=2018,
                                                               watch_condition='VERY_GOOD',
                                                               watch_size=39,
                                                               watch_case_material='STAINLESS_STEEL',
                                                               watch_bracelet_material='STAINLESS_STEEL',
                                                               watch_seller=self.created_seller)
        self.watch.save()

        self.watch2 = WatchSaleAnnouncementModel.objects.create(watch_brand='Rolex',
                                                                watch_model='DayDate',
                                                                watch_price=25000.0,
                                                                watch_photos=self.image_path,
                                                                watch_year=2015,
                                                                watch_condition='VERY_GOOD',
                                                                watch_size=36,
                                                                watch_case_material='STAINLESS_STEEL',
                                                                watch_bracelet_material='STAINLESS_STEEL',
                                                                watch_seller=self.created_seller2)
        self.watch2.save()

        self.review = ReviewModel.objects.create(review_seller_rating=4,
                                                 review_title='Bell\'orologio',
                                                 review_content='È davvero bello',
                                                 review_author=self.created_user,
                                                 review_seller=self.created_seller)

        self.review2 = ReviewModel.objects.create(review_seller_rating=5,
                                                  review_title='Bell\'orologio',
                                                  review_content='È davvero bello',
                                                  review_author=self.created_seller2,
                                                  review_seller=self.created_seller)

    def test_review_view(self):
        client = Client()
        response = client.get(reverse_lazy('reviews:seller-reviews', kwargs={'pk': self.created_seller.pk}))

        self.assertGreaterEqual(len(response.context['object_list']), 1)

    def test_no_reviews(self):
        client = Client()
        response = client.get(reverse_lazy('reviews:seller-reviews', kwargs={'pk': self.created_seller2.pk}))

        self.assertEqual(len(response.context['object_list']), 0)

        self.assertContains(response, 'No reviews yet!')

    def test_create_review(self):
        client = Client()
        response = client.login(username='provauser', password='tentativo')
        self.assertTrue(response)

        response = client.post(reverse_lazy('reviews:leave-review', kwargs={'pk': self.created_seller.pk}),
                               {
                                   'review_seller_rating': 5,
                                   'review_title': 'Molto orologio',
                                   'review_content': 'È davvero molto bello',
                               },
                               follow=True)

        self.assertEqual(response.status_code, 200)

        response = client.get(reverse_lazy('reviews:seller-reviews', kwargs={'pk': self.created_seller.pk}))

        self.assertGreaterEqual(len(response.context['object_list']), 2)

    def test_invalid_rating(self):
        client = Client()
        response = client.login(username='provauser', password='tentativo')
        self.assertTrue(response)

        response = client.post(reverse_lazy('reviews:leave-review', kwargs={'pk': self.created_seller.pk}),
                               {
                                   'review_seller_rating': 7,
                                   'review_title': 'Molto orologio',
                                   'review_content': 'È davvero molto bello',
                               },
                               follow=True)

        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'is-invalid')

    def test_update_review(self):
        client = Client()
        response = client.login(username='provauser', password='tentativo')
        self.assertTrue(response)

        response = client.post(reverse_lazy('reviews:review-update', kwargs={'pk': self.review.pk}),
                               {
                                   'review_seller_rating': 5,
                                   'review_title': 'Titolo modificato',
                                   'review_content': 'È davvero molto bello',
                               },
                               follow=True)

        self.review.refresh_from_db()

        self.assertContains(response, 'Titolo modificato')

    def test_delete_review(self):
        client = Client()
        response = client.login(username='provauser', password='tentativo')
        self.assertTrue(response)

        response = client.post(reverse_lazy('reviews:review-delete', kwargs={'pk': self.review.pk}), follow=True)

        self.assertEqual(response.status_code, 200)

        response = client.get(reverse_lazy('reviews:seller-reviews', kwargs={'pk': self.created_seller.pk}))

        self.assertEqual(len(response.context['object_list']), 1)

    def test_average_reviews(self):
        client = Client()

        response = client.get(reverse_lazy('reviews:seller-reviews', kwargs={'pk': self.created_seller.pk}))

        self.assertEqual(response.context['seller_average_rating'],
                         (self.review.review_seller_rating + self.review2.review_seller_rating)/2)

    def test_user_no_reviews(self):
        client = Client()

        response = client.get(reverse_lazy('reviews:seller-reviews', kwargs={'pk': self.created_user.pk}))

        self.assertEqual(response.status_code, 403)
